---
title: "Assignment 2"
output: html_notebook
---
## 2.1 Cancer-Screening

### a. Wie hoch ist die Wahrscheinlichkeit, dass eine Person an dieser Krebsart tatsächlich erkrankt ist, wenn der Test ein positives Ergebnis (=erkrankt) erbracht hat?

**T** = *Test*  
**D** = *Disease*
$$
P(D\vert T)=\frac{P(T\vert D)\ast P(D)}{P(T\vert D)\ast P(D)+P(T\vert\neg D)\ast P(\neg D)}
$$
Angabe:  
P(**T**|**D**) = 0.9 *(Sensitivität)*  
P(**\~T**|**\~D**) = 0.95 *(Spezifität)*  
  
P(**D**) = 0.001  
P(**T**|**\~D**) = 1 - P(**\~T**|**\~D**) -> 1 - 0.95 = 0.05  
P(**D\~**) = 1 - P(**D**) -> 1 - 0.001 = 0.999  
```{r}
(0.9*0.001)/(0.9*0.001+0.05*0.999)
```

### b. Zeigt das Beispiel, dass Screening ist effizient in der Krebserkennung?

Das Screening ist insofern effizient als dass es 90% der Personen mit Krebserkrankung identifiziert. Für Personen, die positiv diagnostiziert wird, sieht die Sache subjektiv ein wenig anders aus da es sich in den meisten Fällen als falsch herausstellt. Das liegt daran, dass der relative Anteil an Personen, die tatsächlich Krebs haben, sehr gering ist.

## 2.2 Bayes sei Dank

```{r message=FALSE, warning=FALSE}
library(rjags)

bayes_density = function(x, n = 100, title = '') {
  # JAG-Modell für Bayes-Ansatz
  model1.string <- "
  model {
    for (i in 1:N) {
      X[i] ~ dbern(theta)
    }
    theta ~ dunif(0,1)
  }"
  
  # technische Umsetzung mit JAGS
  bayes1.spec <- textConnection(model1.string)
  
  myJags <- jags.model(bayes1.spec, data = list('X' = x, 'N' = length(x)))
  str(myJags)
  
  # simulation der A-POSTERIOR-Daten
  samp <-
    coda.samples(
      myJags,
      variable.names = c("theta"),
      n.iter = n,
      progress.bar = "none"
    )
  summary(samp)
  
  plot(density(samp[[1]]), main = paste("Dichte der Simulation von Theta", title))
  
  mcmc <- samp[[1]]
  mue <- mean(mcmc)
  stdabw <- sd(mcmc)
  return(samp)
}
# fehlerhafter Produktionsanteil - simulieren
n <- 1000
x_input_1 <- rbeta(n, 0.7, 1.5)
x_output_1 <- bayes_density(x_start, n, " - 1. Durchlauf")
x_input_2 <- c(x_output_1[[1]])
x_output_2 <- bayes_density(x_input_2, n, " - 2. Durchlauf")
```


## 2.3 Ein bisschen Theorie

### a. Man leite daraus das Theorem von Bayes ab.

![Bayes-Herleitung](Bayes-Herleitung.png)

### b. Man zeige, dass bei stochastischer Unabhängigkeit der beiden Ereignisse P(**A**|**B**)=P(**A**) gilt.

![Unabhängigkeit](2.3b.png)

## 2.4 Von Kugeln und Wahrscheinlichkeiten

### a. Wie sieht das Bayes’sche Universum in diesem Fall aus?

$$
\left\{\begin{array}{l}(0,0)\\(1,0)\\(2,0)\\(3,0)\\(4,0)\\(5,0)\\(6,0)\\(7,0)\\(8,0)\\(9,0)\end{array}\right.\left.\begin{array}{r}(0,1)\\(1,1)\\(2,1)\\(3,1)\\(4,1)\\(5,1)\\(6,1)\\(7,1)\\(8,1)\\(9,1)\end{array}\right\}
$$

### b. X bezeichne die Anzahl der roten Kugeln im Kübel. Welcher Wertebereich von X ist möglich?

<div style="font-family:Consolas,Courier New;">
| X | prior |        Y=0        |        Y=1        |
|:-:|:-----:|:-----------------:|:-----------------:|
| 0 |  1/10 | 1/10 x 9/9 = 9/90 | 1/10 x 0/9 = 0/90 |
| 1 |  1/10 | 1/10 x 8/9 = 8/90 | 1/10 x 1/9 = 1/90 |
| 2 |  1/10 | 1/10 x 7/9 = 7/90 | 1/10 x 2/9 = 2/90 |
| 3 |  1/10 | 1/10 x 6/9 = 6/90 | 1/10 x 3/9 = 3/90 |
| 4 |  1/10 | 1/10 x 5/9 = 5/90 | 1/10 x 4/9 = 4/90 |
| 5 |  1/10 | 1/10 x 4/9 = 4/90 | 1/10 x 5/9 = 5/90 |
| 6 |  1/10 | 1/10 x 3/9 = 3/90 | 1/10 x 6/9 = 6/90 |
| 7 |  1/10 | 1/10 x 2/9 = 2/90 | 1/10 x 7/9 = 7/90 |
| 8 |  1/10 | 1/10 x 1/9 = 1/90 | 1/10 x 8/9 = 8/90 |
| 9 |  1/10 | 1/10 x 0/9 = 0/90 | 1/10 x 9/9 = 9/90 |
|   |       |             45/90 |             45/90 |
|   |       |             = 1/2 |             = 1/2 |
</div>

### c. Wie sieht die a-priori-Verteilungsfunktion aus, wenn die Werte von X gleich wahrscheinlich sind?

45/90 -> 1/2

### d. Man berechne die a-posteriori-Wahrscheinlichkeit für jeden möglichen X-Wert.

<div style="font-family:Consolas,Courier New;">
| X | prior | likelihood | prior x likelihood | posterior         |
|:-:|:-----:|:----------:|:------------------:|-------------------|
| 0 |  1/10 |        0/9 |  1/10 x 0/9 = 0/90 | 0/90 / 1/2 = 0/45 |
| 1 |  1/10 |        1/9 |  1/10 x 1/9 = 1/90 | 1/90 / 1/2 = 1/45 |
| 2 |  1/10 |        2/9 |  1/10 x 2/9 = 2/90 | 2/90 / 1/2 = 2/45 |
| 3 |  1/10 |        3/9 |  1/10 x 3/9 = 3/90 | 3/90 / 1/2 = 3/45 |
| 4 |  1/10 |        4/9 |  1/10 x 4/9 = 4/90 | 4/90 / 1/2 = 4/45 |
| 5 |  1/10 |        5/9 |  1/10 x 5/9 = 5/90 | 5/90 / 1/2 = 5/45 |
| 6 |  1/10 |        6/9 |  1/10 x 6/9 = 6/90 | 6/90 / 1/2 = 6/45 |
| 7 |  1/10 |        7/9 |  1/10 x 7/9 = 7/90 | 7/90 / 1/2 = 7/45 |
| 8 |  1/10 |        8/9 |  1/10 x 8/9 = 8/90 | 8/90 / 1/2 = 8/45 |
| 9 |  1/10 |        9/9 |  1/10 x 9/9 = 9/90 | 9/90 / 1/2 = 9/45 |
|   |       |            |        45/90 = 1/2 |         45/45 = 1 |
</div>

## 2.5 Noch mehr Kugeln

<div style="font-family:Consolas,Courier New;">
| X | prior | likelihood | prior x likelihood | posterior         |
|:-:|:-----:|:----------:|:------------------:|-------------------|
| 1 |   1/9 |        0/8 |   1/9 x 0/8 = 0/72 | 1/72 / 1/2 = 0/36 |
| 2 |   1/9 |        1/8 |   1/9 x 1/8 = 1/72 | 2/72 / 1/2 = 1/36 |
| 3 |   1/9 |        2/8 |   1/9 x 2/8 = 2/72 | 3/72 / 1/2 = 2/36 |
| 4 |   1/9 |        3/8 |   1/9 x 3/8 = 3/72 | 4/72 / 1/2 = 3/36 |
| 5 |   1/9 |        4/8 |   1/9 x 4/8 = 4/72 | 5/72 / 1/2 = 4/36 |
| 6 |   1/9 |        5/8 |   1/9 x 5/8 = 5/72 | 6/72 / 1/2 = 5/36 |
| 7 |   1/9 |        6/8 |   1/9 x 6/8 = 6/72 | 7/72 / 1/2 = 6/36 |
| 8 |   1/9 |        7/8 |   1/9 x 7/8 = 7/72 | 8/72 / 1/2 = 7/36 |
| 9 |   1/9 |        8/8 |   1/9 x 8/8 = 8/72 | 9/72 / 1/2 = 8/36 |
|   |       |            |        36/72 = 1/2 |         36/36 = 1 |
</div>