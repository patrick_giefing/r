# Bayes #

Man zeige, dass bei stochastischer Unabhängigkeit der beiden Ereignisse P(A|B)=P(A) gilt.

![Herleitung](./2.3b.png "Herleitung")

![Herleitung](./Bayes-Herleitung.png "Herleitung")