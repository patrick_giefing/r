---
title: "Abgabe"
output: html_notebook
---

```{r message=FALSE, warning=FALSE, include=FALSE}
install.packages('nycflights13')
install.packages('tidyverse')
library('nycflights13')
library('tidyverse')
library('ggplot2')
library('dplyr')
```

## 1.1 Brettspiel
$$
f(x)\left\{\begin{array}{c}\begin{array}{cc}0;&x<0\\1/6;&x\geq0\leq6\\0;&x>0\end{array}\end{array}\right.
$$
```{r}
fair_cube_density_area_func <- function(pos) {
  sides <- 6
  cum <- cumsum(rep(1 / sides, sides))
  return(cum[pos])
}
fair_cube_density_area_func(4)
```
## 1.2 Und noch ein Glücksspiel
```{r}
bsp_12_dataframe <- function() {
  augenzahlen <- c(1,2,3,4,5,6)
  frequencies <- c(5,3,4,16,11,2)
  cum_frequencies <- cumsum(frequencies)
  total_frequencies <- sum(frequencies)
  probability_values <- frequencies / total_frequencies
  cum_probability_values <- cumsum(probability_values)
  df <- data.frame(augenzahlen, frequencies, cum_frequencies, probability_values, cum_probability_values)
  return(df);
}
bsp_12_df <- bsp_12_dataframe()
print(bsp_12_df)
barplot(height = bsp_12_df$probability_values, names = bsp_12_df$augenzahlen, xlab = 'Augenzahl', ylab = 'Wahrscheinlichkeit', ylim = c(0, 0.5), main = 'Dichtefunktion')
plot(bsp_12_df$cum_probability_values, type = 'l', xlab = 'Augenzahl', ylab = 'Wahrscheinlichkeit', main = 'Verteilungsfunktion')
```
```{r}
paste('Erwartungswert: ', sum(bsp_12_df$augenzahlen * bsp_12_df$frequencies) / sum(bsp_12_df$frequencies))
```


## 1.3 Dreiecksdichte
![beispiel-1.3](beispiel-1.3.png)

### Dichtefunktion
$$
\begin{array}{l}f(x)=\frac12\times\left\{\begin{array}{lc}0;&x<0\\\frac{(x-a_1)}{(a_2-a_1)};&x\geq0\leq2\\\frac{(a_3-x)}{(a_3-a_2)};&x>2\leq4\\0;&x>4\end{array}\right.\\\end{array}
$$

$$P\lbrack1,8\leq x\leq2,3\rbrack$$
```{r}
bsp13_x_left <- 0.5-1.8^2/8
bsp13_x_right <-  0.5-(4-2.3)^2/8
bsp13_area <- bsp13_x_left + bsp13_x_right
bsp13_area
```
## 1.4 Noch eine Dichtefunktion
Brechnung der Maximalhöhe. Gesamtfläche ist 1. Die einzelnen Flächen werden zusammenaddiert wobei die Höhe jeweils y ist.
$$
\frac{2y}2+3y+\frac{3y}2=1\;\rightarrow\;5,5y=1\;\rightarrow\;y=\frac2{11}
$$
![beispiel-1.4](beispiel-1.4.png)
$$
f(x)=\frac2{11}\times\left\{\begin{array}{ll}0;&x\leq0\\\frac{(x-a_1)}{(a_2-a_1)};&x>0<2\\1;&x\geq2\leq5\\\frac{(a_4-x)}{(a_4-a_3)};&x>5<8\\0;&x\geq8\end{array}\right.
$$
```{r}
x <- 2/11
f_3 <- 2 * x / 2 + 1 * x
f_3
```
## 1.5 Schnellbahn
```{r}
plot(x = c(0,1,1:20,20,21), y = c(0, 0, rep(1/20,20), 0, 0), xlab = 'Wartezeit in Minuten', ylab = 'Wahrscheinlichkeit',  type = 'l', main = 'Dichtefunktion')
plot(cumsum(rep(1/20,20)), xlab = 'Wartezeit in Minuten', ylab = 'Wahrscheinlichkeit',  type = 'l', main = 'Verteilungsfunktion')
```
## 1.6 Und noch eine Verteilungsfunktion
#### a) Dichtefunktion
```{r}
x_16 <- c(2, 2.5, 3, 3.5, 4)
y_16 <- x_16 / -2 + 2
plot(x= c(1.75, 2, x_16, 4.25), y = c(0, 0, y_16, 0), xlab='x', ylab='Wahrscheinlichkeit', type = 'l', main = 'Dichtefunktion')
```
#### b) Erwartungswert

```{r}
sum(y_16) / length(y_16)
```

#### c) Varianz

```{r}
var(y_16)
```


## 1.7 Dichte oder nicht?
```{r}
integral_grenzen_sum <- (3-4)*3-(2-4)*2
integral_grenzen_sum
```
Fläche im bestimmten Integral = 1 daher ist es möglich, dass es sich hier um eine Dichtefunktion handelt
$$
x=\frac{2\sqrt5}5+2\approx2.89427191
$$

## 1.8 Haushaltseinkommen

#### a) Histogramm
```{r}
hist(x=c(rep(250,17),rep(750,15),rep(1500,9),rep(3000,5),rep(5000,3),rep(8000,1)), breaks = c(0,500,1000,2000,4000,6000,10000), freq = TRUE, main = 'Einkommensverteilung', ylab = 'Häufigkeit', xlab = 'Einkommensbereich')
```
#### b) Verteilungsfunktion
```{r}
hh_labels <- c('0<x<=500', '500<x<=1000', '1000<x<=2000', '2000<x<=4000', '4000<x<=6000', '6000<x<=10000')
hh_frequencies <- c(17,15,9,5,3,1)
hh_frequencies_cum <- cumsum(hh_frequencies)
hh_propabilities <- hh_frequencies_cum / sum(hh_frequencies)
data.frame(hh_labels, hh_frequencies, hh_frequencies_cum, hh_propabilities)
plot(hh_propabilities, type = 'l')
```
#### c) Median
```{r}
median(c(c(rep(250,17),rep(750,15),rep(1500,9),rep(3000,5),rep(5000,3),rep(8000,1))))
```

## 1.9 Dann fliegen wir einmal
#### a. Welche Jahre wurden aufgezeichnet?
```{r}
nycflights13::flights %>% distinct(year)
```
#### b. Sind die Jahre vollständig vorhanden?
```{r}
nycflights13::flights %>% distinct(month, day) %>% count == 365
```
#### c. Wie viele Airlines wurden aufgezeichnet
```{r}
nycflights13::flights %>% distinct(carrier) %>% count
```
#### d. Welche Airline hat die größte erwartete Verspätung bei der Ankunft?
```{r}
nycflights13::flights %>% arrange(desc(arr_delay)) %>% head(1) %>% select(carrier)
```
#### e. Welche Airline hat die "zuverlässigste" Verspätung beim Abflug?
```{r}
nycflights13::flights%>% select(carrier, dep_delay) %>% replace(., is.na(.), 0) %>% group_by(carrier) %>% summarize(mean_dep_delay = mean(dep_delay)) %>% arrange(desc(mean_dep_delay)) %>% head(1) %>% select(carrier)
```
#### f. Man visualisiere die Top 10 der pünktlichsten Fluglinien (inkl. Verfrühungen)
```{r}
most_on_time_airlines <- nycflights13::flights %>% select(carrier, arr_delay) %>% replace(., is.na(.), 0) %>% group_by(carrier) %>% summarize(mean_arr_delay = mean(arr_delay)) %>% arrange(mean_arr_delay) %>% head(10)
ggplot(data = most_on_time_airlines, aes(x = factor(carrier, levels = carrier), y = mean_arr_delay)) +  geom_point(stat = 'identity')
```
## 1.10 Corona - Datenexploration
```{r}
df_covid_austria <- readRDS('FHWN_Covid-19-Austria.RDS')
df_covid_vienna <- readRDS('FHWN_Covid-19-Wien.RDS')
# join dataframes by date
df_covid <- merge(x = df_covid_austria, y = df_covid_vienna, by = 'datum')
# remove na rows
df_covid <- df_covid[complete.cases(df_covid), ]
# rename columns
df_covid <- df_covid %>% rename(infizierte_austria = infizierte.x, infizierte_vienna = infizierte.y)
# convert float value row to integers
df_covid$infizierte_austria <- as.integer(df_covid$infizierte_austria)
df_covid$infizierte_vienna <- as.integer(df_covid$infizierte_vienna)
# difference to previous das
df_covid <- cbind(df_covid, rbind(0, data.frame(diff(as.matrix(df_covid$infizierte_austria)))))
df_covid <- cbind(df_covid, rbind(0, data.frame(diff(as.matrix(df_covid$infizierte_vienna)))))
# rename columns
df_covid <- df_covid %>% rename(infizierte_austria_diff = diff.as.matrix.df_covid.infizierte_austria.., infizierte_vienna_diff = diff.as.matrix.df_covid.infizierte_vienna..)
# look what we have so far
df_covid
```
```{r}
summary(df_covid[c('infizierte_austria','infizierte_vienna', 'infizierte_austria_diff', 'infizierte_vienna_diff')])
```
```{r}
df_covid %>% ggplot() + ggtitle('Infizierte Gesamt') +
  geom_line(aes(datum,infizierte_austria, color='Austria')) +
  geom_line(aes(datum,infizierte_vienna, color='Vienna'))
```
#### Infizierte / Tag - Österreich / Wien
```{r}
df_covid %>% ggplot() + 
  ggtitle('Infizierte / Tag') +
  geom_line(aes(datum,infizierte_austria_diff, color='Austria')) +
  geom_line(aes(datum,infizierte_vienna_diff, color='Vienna'))
```
#### Infizierte / Tag - Verhältnis Österreich / Wien im Vergleich zur Einwohnerzahl (9 Mio. zu 1.9 Mio.)
```{r}
df_covid %>% ggplot(aes(x = ('Datum'), y = ('Verhältnis'))) +
  ggtitle('Infizierte / Tag - Vergleich Österreich / Wien') +
  geom_hline(yintercept = 1) + 
  geom_line(aes(datum,(infizierte_austria_diff / (9/1.9)) / infizierte_vienna_diff, color='Austria / Vienna'))
```
```{r}
lmts <- range(df_covid$infizierte_austria_diff, df_covid$infizierte_vienna_diff)

par(mfrow = c(1, 2))
boxplot(df_covid$infizierte_austria_diff, ylim=lmts, main = 'Neuinfizierungen/Tag Österreich')
boxplot(df_covid$infizierte_vienna_diff, ylim=lmts, main = 'Neuinfizierungen/Tag Wien')
```

