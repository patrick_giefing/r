$('.slide.intro').append($(`<div>
  <h1>Randomized Block Design</h1>
  <br />
  <p>
    <a href="https://www.r-bloggers.com/2009/10/examples-using-r-randomized-block-design/" target="_blank">https://www.r-bloggers.com/2009/10/examples-using-r-randomized-block-design/</a>
	<br /><br />
	gleiches Beispiel zu finden in:
	<ul>
		<li><a href="https://web.ma.utexas.edu/users/mks/384E/rcbd.pdf" target="_blank">https://web.ma.utexas.edu/users/mks/384E/rcbd.pdf</a></li>
		<li><a href="https://www.fox.temple.edu/wp-content/uploads/2016/05/Randomized-Block-Design.pdf" target="_blank">https://www.fox.temple.edu/wp-content/uploads/2016/05/Randomized-Block-Design.pdf</a></li>
	</ul>
  </p>
  <br />
  <h3>Patrick Giefing</h3>
</div>`));

$('.slide.reason').append($(`<div>
  <h1>Wann setzt man Randomized Complete Block Design ein?</h1>
  <br />
  <p>
    Wenn eine bekannte oder vermutete Quelle für Varianz existiert.
  </p>
  <p>
	Innerhalb der Blöcke sind die Bedingungen so homogen wie möglich, zwischen den Blöcken können große Unterschiede existieren.
  </p>
  <p class="ref">http://www2.hawaii.edu/~halina/603/603RCBD.pdf</p>
</div>`));

$('.slide.block-what-you-can').append($(`<div>
  <h1>Aussagen</h1>
  <p>
    "Block what you can, randomize what you cannot."
  </p>
  <p class="ref">http://www2.hawaii.edu/~halina/603/603RCBD.pdf</p>
  <br />
  <p>
	"Probably the most used and useful of the experimental designs."
  </p>
  <br />
  <p>
	"The purpose of grouping experimental units is to have the units in a block as uniform
as possible so that the observed differences between treatments will be largely due to
'true' differences between treatments."
  </p>
  <p class="ref">https://www.ndsu.edu/faculty/horsley/RCBD_(revised).pdf</p>
</div>`));

$('.slide.assumptions').append($(`<div>
	<h1>Annahmen</h1>
	<ul>
		<li>Innerhalb eines Blocks sind die Beobachtungen unabhängig von einander.</li>
		<li>Die Population ist normal verteilt.</li>
		<li>Die Standardabweichungen der Populationen der Blöcke sind gleich.</li>
	</ul>
	<h1>Prüfung der Annahmen</h1>
	<ul>
		<li>Ann. 1: Sichergehen, dass die Samples unabhängig von einander gewählt wurden.</li>
		<li>Ann. 2: Box-Plots nebeneinander oder Normal Q-Q-Plots um Normalverteilung sicherzustellen.</li>
		<li>Ann. 3: Vergleich der Varianzen (gleicher Spread)</li>
	</ul>
	<p class="ref">http://www.people.vcu.edu/~wsstreet/courses/314_20033/Handout.Blocks.pdf</p>
</div>`));

$('.slide.levels').append($(`<div>
  <h1>Levels</h1>
  <table>
	<thead>
		<tr>
			<td>Name des Designs</td>
			<td>Anzahl Faktoren<br />k</td>
			<td>Anzahl an Läufen<br />n</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>2-Faktor RBD</td>
			<td>2</td>
			<td>L1*L2</td>
		</tr>
		<tr>
			<td>3-Faktor RBD</td>
			<td>3</td>
			<td>L1*L2*L3</td>
		</tr>
		<tr>
			<td>4-Faktor RBD</td>
			<td>4</td>
			<td>L1*L2*L3*L4</td>
		</tr>
		<tr>
			<td></td>
			<td>...</td>
			<td></td>
		</tr>
		<tr>
			<td>k-Faktor RBD</td>
			<td>k</td>
			<td>L1*L2*...*Lk</td>
		</tr>
	</tbody>
  </table>
  <p class="ref">http://www2.hawaii.edu/~halina/603/603RCBD.pdf</p>
</div>`));

$('.slide.pros-cons').append($(`<div>
  <h2>Vorteile Block Design</h1>
  <br />
  <p>
    <ul>
		<li>Völlige Flexibilität: beliebige Anzahl (mindestens 2) an Blöcken und Beobachtungen möglich.</li>
		<li>Gruppieren bringt genauere Ergebnisse als Completely Randomized Designs.</li>
	</ul>
  </p>
  <h2>Nachteile Block Design</h1>
  <br />
  <p>
	<ul>
		<li>Nicht gedacht für sehr große Anzahl an Beobachtungen da die Blockgröße zu groß wird.</li>
		<li>Nicht geeignet wenn innerhalb eines Blocks große Varianz vorhanden ist.</li>	
	</ul>
  </p>
  <p class="ref">http://www2.hawaii.edu/~halina/603/603RCBD.pdf</p>
</div>`));


$('.step.last-slide').append($(`<div class="slide" style="width:800px;height:190px;">
  <h2>Ihr wolltet doch alle bloß die Katze sehen</h2>
  <span style="font-size:80%;"><a href="https://unsplash.com/photos/OzAeZPNsLXk" target="_blank">https://unsplash.com/photos/OzAeZPNsLXk</a> - Public Domain</span>
</div>`));

var steps = $('.step');
steps.each(function(ind, elem){
	elem = $(elem);
	x = 0;
	y = ind * 800;
	elem.attr('data-x', x);
	elem.attr('data-y', y);
	elem.append($('<div class="paging">' + (ind + 1) + ' von ' + steps.length + '</div>'));
});