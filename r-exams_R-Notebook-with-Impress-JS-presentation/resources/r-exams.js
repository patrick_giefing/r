$('.slide.intro').append($(`<div>
  <h1>R-Exams</h1>
  <h2>From Static to Numeric to<br />Single-Choice Exercises in R/exams</h2>
  <br />
  <p>
    <strong>R-Blog Beitrag</strong><br />
    <a href="https://www.r-bloggers.com/2017/10/from-static-to-numeric-to-single-choice-exercises-in-rexams/" target="_blank">https://www.r-bloggers.com/2017/10/from-static-to-numeric-to-single-choice-exercises-in-rexams/</a>
  </p>
  <br />
  <p>
	<strong>ImpressJS-Integration in R-Notebook</strong>
	<a href="https://bitbucket.org/patrick_giefing/r/wiki/Home" target="_blank">https://bitbucket.org/patrick_giefing/r/wiki/Home</a>
  </p>
  <br />
  <h3>Patrick Giefing</h3>
</div>`));

$('.slide.agenda').append($(`<div>
  <h2>Agenda</h2>
  <ul>
	<li>Motivation</li>
	<li>Formate</li>
	<li>statische Aufgaben</li>
	<li>dynamische Aufgaben</li>
	<li>Skeleton</li>
	<li>selbst entwickelte Beispiele</li>
	<li>HTML-Output</li>
	<li>Moodle-Output in Container</li>
  </ul>
</div>`));

$('.slide.motivation').append($(`<div>
  <h2>Motivation</h2>
  <ul>
	<li>Test-Generierung für Ökonomie-Einführungskurs</li>
	<li>Wiederverwendung für nachfolgende Semester mit möglichst geringem Aufwand</li>
  </ul>
</div>`));

$('.slide.exercise-types').append($(`<div>
  <h2>Typen von Aufgabenstellungen</h2>
  <table class="types">
	<tr>
		<td class="img"><img src="http://www.r-exams.org/images/dynamic-schoice.svg" /></td>
		<td class="type">schoice</td>
		<td>Single-Choice</td>
	</tr>
	<tr>
		<td class="img"><img src="http://www.r-exams.org/images/dynamic-mchoice.svg" /></td>
		<td class="type">mchoice</td>
		<td>Multiple-Choice</td>
	</tr>
	<tr>
		<td class="img"><img src="http://www.r-exams.org/images/dynamic-num.svg" /></td>
		<td class="type">num</td>
		<td>Numerisch (mit Toleranzintervall)</td>
	</tr>
	<tr>
		<td class="img"><img src="http://www.r-exams.org/images/dynamic-string.svg" /></td>
		<td class="type">string</td>
		<td>Zeichenkette (exakte Eingabe)</td>
	</tr>
	<tr>
		<td class="img"><img src="http://www.r-exams.org/images/dynamic-cloze.svg" /></td>
		<td class="type">cloze</td>
		<td>Cloze (Kombination der oben angeführten Typen)</td>
	</tr>
	<tr>
		<td colspan="3" class="sources">
			Quelle Grafiken: <a href="http://www.r-exams.org/intro/dynamic/" target="_blank">http://www.r-exams.org/intro/dynamic/</a>
		</td>
	</tr>
  </table>
</div>`));

$('.slide.question-format-static').append($(`<div>
  <h2>Question-Format</h2>
  <table style="font-size:70%;font-family:Consolas;">
    <tr><td><strong>Markdown</strong></td><td><strong>LaTeX</strong></td></tr>
    <tr>
      <td><img src="./resources/template-rmd.svg" style="width:30%;" /></td>
      <td><img src="./resources/template-rnw.svg" style="width:30%;" /></td>
    </tr>
    <tr>
      <td class="code">
library("exams")
exams2html("examFile.Rmd")
exams2pdf("examFile.Rmd")
      </td>
      <td class="code">
library("exams")
exams2html("examFile.Rnw")
exams2pdf("examFile.Rnw")
      </td>
    </tr>
	<tr>
		<td colspan="3" class="sources">
			Quelle Grafiken: <a href="http://www.r-exams.org/tutorials/first_steps/" target="_blank">http://www.r-exams.org/tutorials/first_steps/</a>
		</td>
	</tr>
  </table>
</div>`));

$('.slide.question-format-static-2').append($(`<div>
  <h2>Question-Format statisch</h2>
  <table style="font-size:70%;font-family:Consolas;vertical-align:top;">
	<tr>
		<td>Markdown</td>
		<td>&nbsp;</td>
		<td>LaTeX</td>
	</tr>
	<tr>
		<td>
<pre>
Question
========
What is the seat of the federal
authorities in Switzerland
(i.e., the de facto capital)?

Answerlist
----------
* Basel
* Bern
* Geneva
* Lausanne
* Zurich
* St. Gallen
* Vaduz
</pre>
		</td>
		<td></td>
		<td>
<pre>
\\begin{question}
What is the seat of the federal
authorities in Switzerland
(i.e., the de facto capital)?

\\begin{answerlist}
  \\item Basel
  \\item Bern
  \\item Geneva
  \\item Lausanne
  \\item Zurich
  \\item St.~Gallen
  \\item Vaduz
\\end{answerlist}
\\end{question}
</pre>
		</td>
	</tr>
  </table>
</div>`));

$('.slide.answer-format-static').append($(`<div>
<h2>Answer-Format statisch</h2>
  <table style="font-size:70%;font-family:Consolas;vertical-align:top;">
	<tr>
		<td>Markdown</td>
		<td>&nbsp;</td>
		<td>LaTeX</td>
	</tr>
	<tr>
		<td>
			<pre>
Solution
========
There is no de jure capital but
the de facto capital and seat of
the federal authorities is Bern.

Answerlist
----------
* False
* True
* False
* False
* False
* False
* False
			</pre>
		</td>
		<td></td>
		<td>
			<pre>
\\begin{solution}
There is no de jure capital but
the de facto capital and seat of
the federal authorities is Bern.

\\begin{answerlist}
  \\item False.
  \\item True.
  \\item False.
  \\item False.
  \\item False.
  \\item False.
  \\item False.
\\end{answerlist}
\\end{solution}
			</pre>
		</td>
	</tr>
  </table>
</div>`));

$('.slide.question-format-dynamic').append($(`<div>
<h2>Question-Format parametrisiert</h2>
  <table style="font-size:70%;font-family:Consolas;vertical-align:top;">
	<tr>
		<td>Markdown</td>
		<td>&nbsp;</td>
		<td>LaTeX</td>
	</tr>
	<tr>
		<td>
			<pre>
Question
========
What is the derivative of
$f(x) = x^{` + '`' + `r a` + '`' + `} e^{` + '`' + `r b` + '`' + ` x}$,
evaluated at $x = ` + '`' + `r c` + '`' + `$?
			</pre>
		</td>
		<td>&nbsp;</td>
		<td>
			<pre>
\\begin{question}
What is the derivative of
$f(x) = x^{\\Sexpr{a}} e^{\\Sexpr{b}x}$,
evaluated at $x = \\Sexpr{c}$?
\\end{question}
			</pre>
		</td>
	</tr>
  </table>
</div>`));

$('.slide.answer-format-dynamic').append($(`<div>
<h2>Answer-Format parametrisiert</h2>
  <table style="font-size:70%;font-family:Consolas;vertical-align:top;">
	<tr>
		<td>Markdown</td>
		<td>&nbsp;</td>
		<td>LaTeX</td>
	</tr>
	<tr>
		<td>
			<pre>
` + '```' + `{r, echo=FALSE, results="hide"}
## parameters
a <- sample(2:9, 1)
b <- sample(seq(2, 4, 0.1), 1)
c <- sample(seq(0.5, 0.8, 0.01), 1)
## solution
res <- exp(b * c) * (a * c^(a-1) +
       b * c^a)
` + '```' + `
			</pre>
		</td>
		<td>&nbsp;</td>
		<td>
			<pre>
<<echo=FALSE, results=hide>>=
## parameters
a <- sample(2:9, 1)
b <- sample(seq(2, 4, 0.1), 1)
c <- sample(seq(0.5, 0.8, 0.01), 1)
## solution
res <- exp(b * c) * (a * c^(a-1) +
       b * c^a)
@
			</pre>
		</td>
	</tr>
  </table>
</div>`));

$('.slide.question-example-dynamic').append($(`<div>
	<h2>Question Example dynamic</h2>
	<img src="./resources/fruits.png" />
</div>`));

$('.slide.exercices').prepend($(`<div>
	<h2>Aufgabenliste erstellen</h2>
	Skeleton mit Beispiel-Aufgaben erstellen
</div>`));

$('.slide.exercice-libs').append($(`<div>
	<h2>selbst entwickelte Beispiele - eingesetzte Bibliotheken</h2>
	<ul>
		<li>exams</li>
		<li>rvest</li>
		<li>xml2</li>
		<li>dplyr</li>
		<li>rworldmap</li>
	</ul>
</div>`));

$('.slide.exercice-aoe').append($(`<div>
	<h2>Beispiel Uralt-Spiel</h2>
	<img src="./resources/aoe-excercise.png" />
</div>`));

$('.slide.exercice-aoe-2').append($(`<div>
	<h2>Beispiel Uralt-Spiel (2)</h2>
	<table>
		<tr>
			<td>
				<img src="./resources/aoe-units.png" style="height:350px;" />
			</td>
			<td>
				<img src="./resources/aoe-units-2.png" style="height:350px;" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				Table: <a href="https://www.unitstatistics.com/age-of-empires2/" target="_blank">https://www.unitstatistics.com/age-of-empires2/</a><br />
				Optionale Icons (nicht alle verfügbar): <a style="font-size:85%;" href="https://ageofempires.fandom.com/wiki/Units_(Age_of_Empires_II)" target="_blank">https://ageofempires.fandom.com/wiki/Units_(Age_of_Empires_II)</a>
			</td>
		</tr>
	</table>
</div>`));

$('.slide.exercice-map').append($(`<div>
	<h2>Beispiel Map</h2>
	<img src="./resources/map-exercise.png" />
</div>`));

$('.slide.exercice-map-2').append($(`<div>
	<h2>Beispiel Map (2)</h2>
	<ul>
		<li>Zufälliges Land &gt; 50.000 km²</li>
		<li>Max. 5 zusätzliche Länder aus derselben Region mit der gleichen Größeneinschränkung</li>
	</ul>
</div>`))

$('.slide.generate-html').prepend($(`<div>
	<h2>HTML generieren</h2>
</div>`));

$('.generate-moodle').prepend($(`<div>
	<h2>Moodle QuestBank-File generieren</h2>
</div>`));


$('.generate-moodle').append($(`<div>
	<img src="./resources/moodle-import.png" style="width:80%;height:80%;" />
</div>`));

$('.slide.output-formats').append($(`<div>
  <h2>Output-Formate</h2>
  <ul>
    <li>HTML</li>
    <li>DOCX</li>
    <li>PDF</li>
    <li>ODF</li>
    <li>ILIAS</li>
    <li>Moodle</li>
    <li>...</li>
  </ul>
</div>`));

$('.slide.moodle-docker').append($(`<div>
  <h2>Moodle\@Docker</h2>
  <a href="https://hub.docker.com/r/bitnami/moodle/" target="_blank">https://hub.docker.com/r/bitnami/moodle/</a><br />
YAML-File herunterladen und docker-compose ausführen
<div class="code" style="font-size:60%;">
curl -sSL https://raw.githubusercontent.com/bitnami/bitnami-docker-moodle/master/docker-compose.yml &gt; docker-compose.yml<br />
docker-compose up -d
</div>
<br />
<table>
	<tr>
		<td>Moodle aufrufen</td>
		<td>&nbsp;:&nbsp;</td>
		<td><a href="http://moodle.docker.local:8080/?redirect=0" target="_blank">http://moodle.docker.local:8080</a></td>
	</tr>
	<tr>
		<td>Default-User</td>
		<td>&nbsp;:&nbsp;</td>
		<td><strong>user</strong></td>
	</tr>
	<tr>
		<td>Default-Passwort</td>
		<td>&nbsp;:&nbsp;</td>
		<td><strong>bitnami</strong></td>
	</tr>
</table>
<em style="color:red;font-size:50%;">Die folgenden Links funktionieren beim Nachbilden des Beispiels nicht da die Ids nicht ident sein werden</em><br />
<table>
	<tr>
		<td><a href="http://moodle.docker.local:8080/question/edit.php?courseid=2" target="_blank">Question Bank</a></td>
		<td>&nbsp;-&nbsp;</td>
		<td class="right"><a href="http://moodle.docker.local:8080/question/import.php?courseid=2" target="_blank">importieren</a></td>
	</tr>
	<tr>
		<td><a href="http://moodle.docker.local:8080/mod/quiz/view.php?id=4" target="_blank">R Quiz 1</a></td>
		<td>&nbsp;-&nbsp;</td>
		<td class="right"><a href="http://moodle.docker.local:8080/mod/quiz/view.php?id=5" target="_blank">R Quiz 2</a></td>
	</tr>
</table>
</div>`));

$('.step.last-slide').append($(`<div class="slide" style="width:800px;height:190px;">
  <h2>Ihr wolltet doch alle bloß die Katze sehen</h2>
  <span style="font-size:80%;"><a href="https://unsplash.com/photos/OzAeZPNsLXk" target="_blank">https://unsplash.com/photos/OzAeZPNsLXk</a> - Public Domain</span>
</div>`));

$('.install-slide').prepend($(`<div>
<h2>R-Exams Bibliothek installieren/laden</h2>
<br /><strong>RTools installieren</strong>:<br /><a href="https://cran.r-project.org/bin/windows/Rtools/" target="_blank">https://cran.r-project.org/bin/windows/Rtools/</a><br />
<br /><strong>RStudio neu starten (RTools wird sonst auch nach Installation noch nicht gefunden)</strong>
</div>`));

$('.step').each(function(ind, elem){
	elem = $(elem);
	x = 0;
	y = ind * 800;
	elem.attr('data-x', x);
	elem.attr('data-y', y);
});